<?php
/**
 * POPO でのPHP Class表現
 * 
 * Todo: 用語集で正しい名前を付ける
 *       e.g) × Vars ○ Property
 */
namespace Text\PHP{
  class Ast
  {
    protected $tree = array();
    
    public function __construct()
    {
    }
  }

  class Node
  {
    public $lineno;
    
    public function __construct($lineno)
    {
      $this->lineno = $lineno;
    }
    
    public function compile($compiler)
    {
    }
  }

  class NodeList extends Node
  {
    public $nodes;
    
    public function __construct($nodes, $lineno =1)
    {
      parent::__construct($lineno);
      $this->nodes = $nodes;
    }
    
    public function compile($compiler)
    {
      foreach($this->nodes as $node)
        $node->compile($compiler);
    }
    
    public function fromArray($array, $lineno)
    {
      if(count($array) ==1)
        return $array[0];
      return new NodeList($array,$lineno);
    }
  }
  
  class Namespaces extends Node
  {
    public $_namespace;
    public $nodes;
    
    public function __construct($namespace, $nodes)
    {
      $this->_namespace = $namespace;
      $this->nodes = $nodes;
    }

  }
  
  /**
   * PHP Class Tree
   */
  class Classes extends Node
  {
    public $_classname;

    public function __construct($classname,$methods)
    {
      $this->_classname = $classname;
      $this->_methods = $methods;
    }
  }
  
  class Method extends Node
  {
    public $_method;
    
    public function __construct($method, $params, $code)
    {
      $this->_method = $method;
      $this->_params = $params;
      $this->_code = $code;
    }
  }

  /**
   * PHP Class Properties
   */
  class Properties
  {
    protected $_type;
    protected $_doccomment;
    protected $_name;
    protected $_default;
  }

  /**
   * PHP Class Constants
   */
  class Constants
  {
    protected $_doccomment;
    protected $_name;
    protected $_value;
  }
  
  class MethodParams extends Node
  {
    public $_name;
    public function __construct($param_name)
    {
      $this->name = $param_name;
    }
  }
  
  class Code extends Node
  {
    public $data;
    public function __construct($data)
    {
      $this->data = $data;
    }
  }
}


/**
 * 名前空間未定義の。
 */

namespace{
  /**
   * めんどくさいので定義しておく
   *
   */
  define("T_UNDEFINED", 0);
  define("T_TERMINAL",999);
  define("T_LEFT_PAREN",1000);
  define("T_RIGHT_PAREN",1001);
  define("T_LEFT_BRACE",1002);
  define("T_RIGHT_BRACE",1003);

  class Token
  {
    protected $type = 0;
    protected $data = "";
    protected $line = 0;
    
    public function __construct($array)
    {
      if(is_array($array)){
        $this->type = $array[0];
        $this->data = $array[1];
        $this->line = $array[2];
      }else{
        switch($array){
          case ";":
            $this->type = T_TERMINAL;
            break;
          case "(":
            $this->type = T_LEFT_PAREN;
            break;
          case ")":
            $this->type = T_RIGHT_PAREN;
            break;
          case "{":
            $this->type = T_LEFT_BRACE;
            break;
          case ")":
            $this->type = T_RIGHT_BRACE;
            break;
          default:
            $this->type = 0;
            break;
        }

        $this->data = $array;
        $this->line = 0;
      }
    }
    
    public function getType()
    {
      return $this->type;
    }
    
    public function getData()
    {
      return $this->data;
    }
    public function getLine()
    {
      return $this->line;
    }
  }

  class TokenStream
  {
    private $tokens;
    private $offset = 0;
    private $count = 0;
    
    public function __construct($string)
    {
      $i = 0;
      $tokens = token_get_all($string);

      foreach($tokens as $token){
        $this->tokens[] = new Token($token);
        $i++;
      }
      
      $this->count = $i;
    }
    
    public function back()
    {
      $this->offset--;
      return $this;
    }
    
    public function getToken()
    {
      if(isset($this->tokens[$this->offset])){
        $token = $this->tokens[$this->offset];
        $this->offset++;
        return $token;
      }else{
        return false;
      }
    }
    
    public function valid()
    {
      if(isset($this->tokens[$this->offset])){
        return true;
      }else{
        return false;
      }
    }
  }


  /**
   * Machineは状態遷移の管理をする。
   *
   * Todo: パースした結果はPOPでのクラス表現に返したいな。
   */
  abstract class Machine
  {
    protected $machine;


    protected function getRootMachine()
    {
      return $this->machine;
    }

    protected function setRootMachine(Machine $machine)
    {
      $this->machine = $machine;
    }
    
    protected function execMachine($machineName,TokenStream &$token)
    {
      $machine = new $machineName();
      $machine->setRootMachine($this);
      $res = $machine->exec($token);
      return $res;
    }

    public function __construct()
    {
    }
    
    /**
     * 今回外側に通知する必要はなさそうな気配
     */
    public function notify($obj)
    {
      $notifier = $this->notifier;
      $notifier($obj);
    }
    
    abstract function exec(TokenStream $token, $options = array());
  }

  class Machine_Params extends Machine
  {
    const INIT = 0;
    const IN_PARAMS = 1;

    public function exec(TokenStream $token, $options = array())
    {
      $state = self::INIT;
      $prefix = null;
      $paren = 0;

      while($token->valid()){
        $current = $token->getToken();
        $type = $current->getType();
        switch($state){
          case self::INIT:
            if($type == T_LEFT_PAREN){
              $paren++;
              $state = self::IN_PARAMS;
            }else{
              throw new Exception();
            }
            break;
            
          case self::IN_PARAMS:
            if($type == T_LEFT_PAREN){
              $paren++;
            }else if($type == T_RIGHT_PAREN){
              $paren--;
              
              if($paren == 0){
                return new Text\PHP\MethodParams("1");
              }
            }else{
              // nothing to do.
              // Todo: ちゃんとパラメーターひろう
            }
            break;
        }
      }
    }
  }

  class Machine_Block extends Machine
  {
    const INIT = 0;
    protected $_data;

    public function exec(TokenStream $token, $options = array())
    {
      $state = self::INIT;
      $paren = 0;
      
      while($token->valid()){
        $current = $token->getToken();
        $type = $current->gettype();
        if($type == T_LEFT_BRACE){
          $paren++;
        }else if($type == T_RIGHT_BRACE){
          $paren--;
          if($paren == 0){
            $this->_data .= $current->getData();
            return $this;
          }
        }
        $this->_data .= $current->getData();
      }
      
      return $this->_data;
    }

  }

  class Machine_Method extends Machine
  {
    const INIT = 0;
    const METHOD_DEFINITION = 1;
    const METHOD_PARAMETERS = 2;
    const DATA = 3;

    protected $_name;
    protected $_params;

    public function exec(TokenStream $token, $options = array())
    {
      $state = self::INIT;
      $prefix = null;
      $r = array();

      while($token->valid()){
        $current = $token->getToken();
        $type = $current->getType();
        
        switch($state){
          case self::INIT:
            if($type == T_FUNCTION){
              $state = self::METHOD_DEFINITION;
            }
            break;
            
         case self::METHOD_DEFINITION:
           if($type == T_STRING){
             $methodname = $current->getData();
             $tmp = array();
             $state = self::METHOD_PARAMETERS;
           }else if($type == T_WHITESPACE){
             // nothing to do.
           }else{
             throw new Exception();
           }
           break;
           
         case self::METHOD_PARAMETERS:
           if($type == T_WHITESPACE){
             // nothing to do.
           }else if($type == T_LEFT_PAREN){
             $tmp[] = $this->execMachine("Machine_Params",$token->back());
             $state = self::DATA;
           }else{
             throw new Exception();
           }
           break;
           
         case self::DATA:
           if($type == T_LEFT_BRACE){
             $data = $this->execMachine("Machine_Block",$token->back());
             return new Text\PHP\Method($methodname,new Text\PHP\NodeList($tmp), new Text\PHP\Code($data));
           }else if($type == T_WHITESPACE){
             //nothing to do.
           }else{
             throw new Exception();
           }
           break;
        }
      }

    }
  }

  class Machine_Vars extends Machine
  {
    public function exec(TokenStream $token, $options = array())
    {
    }
  }


  class Machine_Class extends Machine
  {
    const INIT = 0;
    const IN_CLASS = 1;

    protected $_class_type = "public";
    protected $_class_name;
    protected $_vars = array();
    protected $_methods = array();

    public function exec(TokenStream $token, $options = array())
    {
      $state = self::INIT;
      $prefix = null;
      $r = array();

      while($token->valid()){
        $current = $token->getToken();
        $type = $current->getType();
        
        switch($state){
          case self::INIT:
            if($type == T_STRING){
              $class = $current->getData();
              $state = self::IN_CLASS;
            }
            break;

          case self::IN_CLASS;
            if($type == T_FUNCTION){
              $method = $this->execMachine("Machine_Method",$token->back());
              $r[] = $method;
            }else if($type == T_WHITESPACE){
              // nothing to do.
            }else{
              //throw new exception();
            }
            break;
        }
        
        // Todo: 実装方法要検討。
        if($type == T_PUBLIC || $type == T_PROTECTED || $type == T_PRIVATE){
          $prefix = $current;
        }
      }
      return new Text\PHP\Classes($class,new Text\PHP\NodeList($r),1);
      //var_dump($this);
    }
  }

  class Machine_PHP extends Machine
  {
    const INIT = 0;
    const IN_NAMESPACE = 1;
    
    protected $_namespace;
    
    public function __construct()
    {
      $this->setRootMachine($this);
    }
    
    public function exec(TokenStream $token, $options = array())
    {
      $state = self::INIT;
      $r = array();
      
      while($token->valid())
      {
        $current = $token->getToken();
        $type = $current->getType();

        switch($state){
          case self::INIT:
            /**
             * 考え方的には必ずMachine_Namespaceは起動されて
             * Globalもしくは指定名前空間てなるはず。
             */
            if($type == T_NAMESPACE){
              $state = self::IN_NAMESPACE;
            }else if($type == T_ABSTRACT || $type == T_INTERFACE){
              $class[] = $this->execMachine("Machine_Class",$token->back());
            }else if($type == T_CLASS){
              $token->back();
              $class[] = $this->execMachine("Machine_Class",$token->back());
            }
            break;

          // 適当namespace parse
          // Machine_Namespaceとかちゃんとやるなら必要
          case self::IN_NAMESPACE:
            if($type == T_STRING){
              $this->_namespace .= $current->getData();
            }else if($type == T_NS_SEPARATOR){
              $this->_namespace .= $current->getData();
            }else if($type == T_WHITESPACE){
              // nothing to do.
            }else if($type == T_TERMINAL){
              $r[] = new Text\PHP\Namespaces($this->_namespace,$class);
              $state = self::INIT;
            }else{
              $r[] = new Text\PHP\Namespaces($this->_namespace,$class);
              $state = self::INIT;
            }
            break;
        }
        
        $previous = $current;
      }
      return new Text\PHP\NodeList(new Text\PHP\Namespaces($this->_namespace,new Text\PHP\NodeList($class)));
    }
  }

  class Machine_File extends Machine
  {
    public function exec(TokenStream $token, $options = array())
    {
      $r = array();
      while($token->valid())
      {
        $current = $token->getToken();
        switch($current->getType()){
          case T_OPEN_TAG:
            $nodes = $this->execMachine("Machine_PHP",$token);
            if($nodes)
              $r[] = $nodes;
            break;
          default:
            // nothing to do.
            break;
        }
        
        $previous = $current;
      }
      
      var_dump($r);
    }
  }




// token 一覧 http://jp.php.net/manual/ja/tokens.php
$code = <<<PHP_CODE
<?php
namespace Uhi\Moe\Ana;

class ExampleClass
{
    public \$attribute;

    /**
     * Hello World
     *
     *
     */
    public function doSomeMethod (\$a = 0)
    {
      var_dump(1);
      
    }
}
PHP_CODE;

$token = new TokenStream($code);

$ast = null;
$machine = new Machine_File(function($event) use ($ast){
  //$ast = $event;
});
$machine->exec($token);

//var_dump($ast);




/**
クラスから出力されるrest案

namespace Classname
==========================

Class Doccomennt.Class Doccomennt.Class Doccomennt
Class Doccomennt.Class Doccomennt.Class Doccomennt
Class Doccomennt.Class Doccomennt.Class Doccomennt
Class Doccomennt.Class Doccomennt.


.. php:class:const:: const_name Line No
   :nantyara: kantyara

.. php:class:vars:: var_name Line No
   :nantyara: kantyara
   
.. php:class:method:: method_name params Line No
   :nantyara: kantyara

*/
}
